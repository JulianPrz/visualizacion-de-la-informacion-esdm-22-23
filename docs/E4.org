#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE:E4 GIS - Sistemas de Representación Geográfica
#+SUBTITLE: Visualización de la Información
#+AUTHOR: Julián Pérez
#+LANGUAGE: es
#+EMAIL:julian.perezromero@educa.madrid.org
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight CopyCode)
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: ../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="r-fit-text" >%t</h1><br><br><h3 class="subtitle">%s</h3><br><h4>%a</h4><br><p>Máster Diseño Interactivo</p><p>Escuela Superior de Diseño de Madrid</p>
#+OPTIONS: toc:nil

* Enunciado
- Para este ejercicio tenemos dos opciones a elegir para desarrollar un mapa interactivo:
  1) hacer un hexmap como el que vimos en clase pero utilizando otro conjunto de datos
  2) Elaborar un mapa propio incluyendo alguna capa de datos geográficos ya elaborada (CSV, Shapefile, etc.)
* Hexmap
- Algunas fuentes de interés para realizar esta práctica:
  - https://www.gislounge.com/using-qgis-create-hexbin-map-gisp-registrations/
  - https://www.youtube.com/watch?v=ETk1pbGTen0
  - https://www.youtube.com/watch?v=ejCK2LbEhdA
- Ejemplos de hexmaps:
  - https://www.theguardian.com/politics/ng-interactive/2016/jun/23/eu-referendum-live-results-and-analysis
  - https://www.flerlagetwins.com/2018/11/what-hex-brief-history-of-hex_68.html
* Mapa propio
- Al igual que hicimos con la primera práctica de GIS se puede
  elaborar un mapa propio para esta práctica
- Utiliza al menos una capa con información geográfica. Ya sean
  elementos de punto, por ejemplo: accidentes, comercios, estaciones
  de carga de coches eléctricos, etc. o elementos vectoriales o
  poligonales, por ejemplo: calles, ríos, edificios, áreas protegidas,
  etc.
- En el [[https://centrodedescargas.cnig.es/CentroDescargas/index.jsp][centro de descargas del Instituto Geográfico Nacional]] (IGN) podéis encontrar muchas capas vectoriales en formato shape
* A tener en cuenta
- Ejercicio individual
- Pregunta si tienes dificultades con la obtención de datos a representar
- Buena estructura de los archivos del proyecto
* Puntos a desarrollar
- Desarrollo de mapa en QGIS
- Incluir datos geográficos de tú interés
- Exportado de mapa con el plugin QGIS2WEB
* Evaluación
- Se valorará la ideación, la elección de datos a representar, la
  calidad estética, cumplir con los requisitos y la entrega a tiempo
* Entrega
- Entrega por el aula virtual del proyecto web que muestre el mapa interactivo
- Entrega: *13 abril*
