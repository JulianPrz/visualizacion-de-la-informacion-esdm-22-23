//http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}
var urlRoot = "http://api.openweathermap.org/"
var apiGeo = "geo/1.0/direct?q="
var city = "Madrid"
var limit = "&limit=1"
var apiKey = "&appid=83e71cb46c30afc0c556cc1837bcfe6b"
var lat, lon
//http://api.openweathermap.org/data/2.5/air_pollution?lat={lat}&lon={lon}&appid={API key}
var apiPol = "data/2.5/air_pollution?"
var input

var cities

function setup(){
    var button = select("#submit")
    button.mousePressed(geoSubmit)
    input = select("#city")
    //loadJSON("https://datos.comunidad.madrid/catalogo/dataset/032474a0-bf11-4465-bb92-392052962866/resource/301aed82-339b-4005-ab20-06db41ee7017/download/municipio_comunidad_madrid.json", "jsonp", getCities)
}

// function getCities(data){
//     for(cities in data.municipio_nombre){
//         print(cities)
//     }
//   //for(let i=0; i < )
// }
function geoSubmit(){
   print(input.value())
   loadJSON(urlRoot+apiGeo+input.value()+limit+apiKey, getGeoData)
}

function getGeoData(data){
    geoData = data
    lat = "lat=" + geoData[0].lat
    lon = "&lon=" + geoData[0].lon
    loadJSON(urlRoot+apiPol+lat+lon+apiKey, getAirPollution)
}

function getAirPollution(data){
    airData = data
    //print(airData)
    let object = data.list[0].components
    let components
    for( components in object){
        print(components+" : "+object[components])
    }
}